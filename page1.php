<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<style>
  .container {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 34vw;
    margin: 2rem 33vw;
    border: 2px solid #385e8b;
    padding: 1rem;
  }

  .questions {
    /* margin-bottom: 6px; */
  }

  .title-question {
    font-weight: bold;
  }

  .answer {
    display: flex;
    justify-content: space-between;
  }

  .answer-child {
    width: 15vw;
    display: flex;
    flex-direction: column;
    justify-items: center;
  }

  .answer-option {
    margin-bottom: 10px;
  }

  .btn-submit {
    margin-top: 10px;
    height: 40px;
  }
</style>

<body>

  <?php
  $title_question = array(
    "1" => array(
      "title" => "Ai là người đã đưa ra chủ trương thiết lập nền 'Chuyên chính cách mạng của những người lao động'?",
      "result" => "D",
      "name" => "question_1",
      "anwser" => array(
        "A" => "Tômát Morơ",
        "B" => "Xanh Ximông",
        "C" => "Grắccơ Babớp",
        "D" => "Morenly"
      ),
    ),
    "2" => array(
      "title" => "Nguyên nhân chủ yếu của những hạn chế của chủ nghĩa xã hội không tưởng là?",
      "result" => "C",
      "name" => "question_2",
      "anwser" => array(
        "A" => "Do trình độ nhận thức của những nhà tư tưởng",
        "B" => "Do khoa học chưa phát triển",
        "C" => "Do những điều kiện lịch sử khách quan quy định",
        "D" => "Cả a, b, c"
      ),
    ),
    "3" => array(
      "title" => "Ai là người được Ph.Ăngghen đánh giá là 'nắm phép biện chứng một cách cũng tài tình như Hêghen là người đương thời với ông'",
      "result" => "C",
      "name" => "question_3",
      "anwser" => array(
        "A" => "Mê li ê",
        "B" => "Xanh Ximông",
        "C" => "Phurie",
        "D" => "Ôoen"
      ),
    ),
    "4" => array(
      "title" => "Ph. Ăngghen đã đánh giá: 'Hai phát hiện vĩ đại này đã đưa chủ nghĩa xã hội trở thành một khoa học'. Hai phát kiến đó là gì?",
      "result" => "C",
      "name" => "question_4",
      "anwser" => array(
        "A" => "Chủ nghĩa duy vật biện chứng và chủ nghĩa duy vật lịch sử",
        "B" => "Sứ mệnh lịch sử của giai cấp công nhân – Học thuyết giá trị thặng dư",
        "C" => "Học thuyết giá trị thặng dư – Chủ nghĩa duy vật lịch sử",
        "D" => "Sứ mệnh lịch sử của giai cấp công nhân – Chủ nghĩa duy vật lịch sử"
      ),
    ),
    "5" => array(
      "title" => "Ai là người nêu ra luận điểm: Trong nền kinh tế tư bản chủ nghĩa, 'sự nghèo khổ được sinh ra từ chính sự thừa thãi'?",
      "result" => "B",
      "name" => "question_5",
      "anwser" => array(
        "A" => "Xanh Ximông",
        "B" => "Sáclơ Phuriê",
        "C" => "Rôbớt Ôoen",
        "D" => "Tômát Morơ"
      ),
    ),
  );
  ?>
  <?php
  $data = array();
  $error = false;
  $cookie_value = 0;
  $cookie_name = "total_results_page1";
  if (!empty($_POST['btnSubmit'])) {
    $data['question_1'] = isset($_POST['question_1']) ? $_POST['question_1'] : '';
    $data['question_2'] = isset($_POST['question_2']) ? $_POST['question_2'] : '';
    $data['question_3'] = isset($_POST['question_3']) ? $_POST['question_3'] : '';
    $data['question_4'] = isset($_POST['question_4']) ? $_POST['question_4'] : '';
    $data['question_5'] = isset($_POST['question_5']) ? $_POST['question_5'] : '';
    foreach ($data as $key => $value) {
      if (empty($value)) {
        $error = true;
      }
    }
    if ($error == true) {
      echo "Chưa chọn hết các câu hỏi";
    } else {
      foreach ($title_question as $key => $value) {
        $name = "question_" . $key;
        if ($value['result'] == $data[$name]) {
          $cookie_value = $cookie_value + 1;
        }
      }
      setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
      $title_question_name = 'title_question_page1';
      setcookie($title_question_name, json_encode($title_question), time() + (86400 * 30), "/");
      $data_answer = 'data_answer_page1';
      setcookie($data_answer, json_encode($data), time() + (86400 * 30), "/");
      header("Location: ./page2.php");
    }
  }
  ?>
  <form method="POST" action="page1.php" id="form">
    <div class="container">
      <div class="questions">
        <?php
        foreach ($title_question as $key => $value) {
          echo '<p class="title-question">Câu ' . $key . ': ' . $value['title'] . '</p>';
          foreach ($value['anwser'] as $keyAns => $valueAns) {
            echo '<div class="answer-option">
              <input type="radio" id="' . $key . '' . $keyAns . '" name="' . $value['name'] . '" value="' . $keyAns . '">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>';
          }
        }
        ?>
      </div>
      <input type="submit" value="Next" class="btn-submit" name="btnSubmit" />
    </div>
  </form>
</body>

</html>