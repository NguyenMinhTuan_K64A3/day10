<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<style>
  .container {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 34vw;
    margin: 2rem 33vw;
    border: 2px solid #385e8b;
    padding: 1rem;
  }

  .questions {
    /* margin-bottom: 6px; */
  }

  .title-question {
    font-weight: bold;
  }

  .answer {
    display: flex;
    justify-content: space-between;
  }

  .answer-child {
    width: 15vw;
    display: flex;
    flex-direction: column;
    justify-items: center;
  }

  .answer-option {
    margin-bottom: 10px;
  }

  .btn-submit {
    margin-top: 10px;
    height: 40px;
  }
</style>

<body>

  <?php
  $title_question = array(
    "6" => array(
      "title" => "Trong khoa học xã hội những thành tựu nào đã làm tiền đề lý luận cho sự ra đời của Chủ nghĩa duy vật biện chứng và Chủ nghĩa duy vật lịch sử?",
      "result" => "D",
      "name" => "question_6",
      "anwser" => array(
        "A" => " Triết học cổ điển Đức",
        "B" => "Kinh tế chính trị học cổ điển Anh",
        "C" => "Chủ nghĩa không tưởng phê phán Anh, Pháp",
        "D" => "Tất cả đều đúng"
      ),
    ),
    "7" => array(
      "title" => "Trong khoa học tự nhiên những phát minh vạch thời đại trong vật lý và sinh?",
      "result" => "D",
      "name" => "question_7",
      "anwser" => array(
        "A" => "Học thuyết tiến hóa",
        "B" => "Định luật bảo toàn và chuyển hóa năng lượng ",
        "C" => "Học thuyết tế bào",
        "D" => "Tất cả đều đúng"
      ),
    ),
    "8" => array(
      "title" => "Những yếu tố tư tưởng XHCN được xuất hiện từ khi nào?",
      "result" => "B",
      "name" => "question_8",
      "anwser" => array(
        "A" => "Chế độ tư bản chủ nghĩa ra đời",
        "B" => "Sự xuất hiện chế độ tư hữu, xuất hiện giai cấp thống trị và bóc lột",
        "C" => "Sự xuất hiện giai cấp công nhân",
        "D" => "Ngay từ thời cộng sản nguyên thuỷ"
      ),
    ),
    "9" => array(
      "title" => "Ai đã đưa ra quan niệm?",
      "result" => "D",
      "name" => "question_9",
      "anwser" => array(
        "A" => "S.Phuriê",
        "B" => "C.Mác",
        "C" => "Ph.Ănghen",
        "D" => "V.I.Lênin"
      ),
    ),
    "10" => array(
      "title" => "Ai được coi là người mở đầu các trào lưu xã hội chủ nghĩa và cộng sản chủ nghĩa thời cận đại?",
      "result" => "B",
      "name" => "question_10",
      "anwser" => array(
        "A" => "Tômađô Campanenla",
        "B" => "Tômát Morơ",
        "C" => "Arítxtốt",
        "D" => "Platôn"
      ),
    ),
  );
  ?>
  <?php
  $data = array();
  $error = false;
  $cookie_value = 0;
  $cookie_name = "total_results_page2";
  if (!empty($_POST['btnSubmit'])) {
    $data['question_6'] = isset($_POST['question_6']) ? $_POST['question_6'] : '';
    $data['question_7'] = isset($_POST['question_7']) ? $_POST['question_7'] : '';
    $data['question_8'] = isset($_POST['question_8']) ? $_POST['question_8'] : '';
    $data['question_9'] = isset($_POST['question_9']) ? $_POST['question_9'] : '';
    $data['question_10'] = isset($_POST['question_10']) ? $_POST['question_10'] : '';
    foreach ($data as $key => $value) {
      if (empty($value)) {
        $error = true;
      }
    }
    if ($error == true) {
      echo "Chưa chọn hết các câu hỏi";
    } else {
      foreach ($title_question as $key => $value) {
        $name = "question_" . $key;
        if ($value['result'] == $data[$name]) {
          $cookie_value = $cookie_value + 1;
        }
      }
      setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
      $title_question_name = 'title_question_page2';
      setcookie($title_question_name, json_encode($title_question), time() + (86400 * 30), "/");      
      $data_answer = 'data_answer_page2';
      setcookie($data_answer, json_encode($data), time() + (86400 * 30), "/");
      header("Location: ./page3.php");
    }
  }
  ?>
  <form method="POST" action="page2.php" id="form">
    <div class="container">
      <div class="questions">
        <?php
        foreach ($title_question as $key => $value) {
          echo '<p class="title-question">Câu ' . $key . ': ' . $value['title'] . '</p>';
          foreach ($value['anwser'] as $keyAns => $valueAns) {
            echo '<div class="answer-option">
              <input type="radio" id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>';
          }
        }
        ?>
      </div>
      <input type="submit" value="Nộp bài" class="btn-submit" name="btnSubmit" />
    </div>
  </form>
</body>

</html>