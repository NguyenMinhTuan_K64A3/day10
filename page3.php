<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css" />
</head>
<style>
  .container {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 34vw;
    margin: 2rem 33vw;
    border: 2px solid #385e8b;
    padding: 1rem;
  }

  .title-question {
    font-weight: bold;
  }

  .answer-option {
    margin-bottom: 10px;
  }

  .bg-color-green {
    color: green;
  }

  .bg-color-red {
    color: red;
  }
</style>

<body>
  <div class="container">
    <?php
    if (isset($_COOKIE['total_results_page1']) && isset($_COOKIE['total_results_page2'])) {
      $result = (int)$_COOKIE['total_results_page1'] + (int)$_COOKIE['total_results_page2'];
      echo "<p style='margin: 0px'>Số câu đúng là:  $result </p>";
      echo "<p>Điểm: $result </p>";
      if ($result < 4) {
        echo "Bạn quá kém, cần ôn tập thêm";
      } else if ($result < 7) {
        echo "Cũng bình thường";
      } else {
        echo "Sắp sửa làm được trợ giảng lớp PHP";
      }
    }
    if (isset($_COOKIE['title_question_page1']) && isset($_COOKIE['title_question_page2'])) {
      $title_question_1 = json_decode($_COOKIE['title_question_page1'], true);
      $title_question_2 = json_decode($_COOKIE['title_question_page2'], true);
      $data_answer_1 = json_decode($_COOKIE['data_answer_page1'], true);
      $data_answer_2 = json_decode($_COOKIE['data_answer_page2'], true);
      foreach ($title_question_1 as $key => $value) {
        echo '<p class="title-question">Câu ' . $key . ': ' . $value['title'] . '</p>';
        $name = "question_" . $key;
        foreach ($value['anwser'] as $keyAns => $valueAns) {
          if ($value['result'] == $keyAns) {
            echo '<div class="answer-option bg-color-green pretty p-default p-round p-thick">
            <input type="checkbox" checked disabled id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
            <div class="state p-success">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>
          </div>';
          } else if ($keyAns == $data_answer_1[$name]) {
            echo '<div class="answer-option bg-color-red pretty p-default p-round p-thick">
            <input type="checkbox" checked disabled id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
            <div class="state p-danger">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>
          </div>';
          } else {
            echo '<div class="answer-option pretty p-default p-round p-thick">
              <input type="checkbox" disabled id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
              <div class="state p-danger">
                <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
              </div>
            </div>';
          }
        }
      }

      foreach ($title_question_2 as $key => $value) {
        echo '<p class="title-question">Câu ' . $key . ': ' . $value['title'] . '</p>';
        $name = "question_" . $key;
        foreach ($value['anwser'] as $keyAns => $valueAns) {
          if ($value['result'] == $keyAns) {
            echo '<div class="answer-option bg-color-green pretty p-default p-round p-thick">
            <input type="checkbox" checked disabled id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
            <div class="state p-success">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>
          </div>';
          } else if ($keyAns == $data_answer_2[$name]) {
            echo '<div class="answer-option bg-color-red pretty p-default p-round p-thick">
            <input type="checkbox" checked disabled id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
            <div class="state p-danger">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>
          </div>';
          } else {
            echo '<div class="answer-option pretty p-default p-round p-thick">
              <input type="checkbox" disabled id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
              <div class="state p-danger">
                <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
              </div>
            </div>';
          }
        }
      }
    }
    ?>
  </div>
</body>

</html>